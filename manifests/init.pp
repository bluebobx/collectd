#
#
#
class bluebobx-collectd {
  package { 'collectd' :
    ensure => present,
  }

  $collectdconf = $::operatingsystem ? {
    Ubuntu => '/etc/collectd/collectd.conf',
    default => '/etc/collectd.conf',
  }

  file { 'collectd_conf' :
    path => $collectdconf,
    ensure => present,
    source => 'puppet:///modules/bluebobx-collectd/collectd.conf',
    require => Package['collectd'],
    notify => Service['collectd'],
  }

  service { 'collectd' :
    ensure => running,
    require => File['collectd_conf'],
  }

}
